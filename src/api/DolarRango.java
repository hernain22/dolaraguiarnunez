package api;

import gobws.GobwsFecha;
import java.io.IOException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/DolarRango/{fechaIn}&{fechaTe}")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DolarRango {

    @GET
    public Response Dolar(@PathParam("fechaIn") String fechaIn, @PathParam("fechaTe") String fechaTe) {
        try {
            GobwsFecha gsf = new GobwsFecha();
            String respuesta=gsf.DolarRango(fechaIn, fechaTe);            
            return Response.ok(respuesta, MediaType.APPLICATION_JSON).build();
        } catch (IOException ex) {
            return Response.ok("Error al consultar.", MediaType.APPLICATION_JSON).build();
        }

    }

}
