package api;

import gobws.Gobws;
import gobws.GobwsFecha;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/DolarHoy")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DolarHoy {

    @GET
    public Response dolarHoy() {

        String resultado = "";
        try {

            Calendar now = Calendar.getInstance();
            int diaSemana = now.get(Calendar.DAY_OF_WEEK);

            String fecha ="";

            SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");

            if (diaSemana == 6) {
                now.add(Calendar.DATE, -1);
                fecha = format1.format(now.getTime());
                GobwsFecha gsf = new GobwsFecha();
                resultado = gsf.DolarRango(fecha, fecha);

            } else if (diaSemana == 1) {
                now.add(Calendar.DATE, -2);
                fecha = format1.format(now.getTime());
                GobwsFecha gsf = new GobwsFecha();
                resultado = gsf.DolarRango(fecha, fecha);

            } else {
                Gobws gs = new Gobws();
                resultado = gs.DolarHoy();
            }

            return Response.ok(fecha + resultado, MediaType.APPLICATION_JSON).build();
        } catch (IOException ex) {
            return Response.ok("Error en dolarHOY", MediaType.APPLICATION_JSON).build();
        }

    }

}
