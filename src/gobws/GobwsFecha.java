package gobws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class GobwsFecha {

    private final String wsURL = "https://api.desarrolladores.datos.gob.cl/indicadores-financieros/v1/dolar/periodo.json/";

    private String key = "10217b03edd7a780a17949d6513215520ca2ec64";

    public String DolarRango(String fechainicio, String fechafin) throws MalformedURLException, IOException {

        String respuesta = "";

        String parametros = "?auth_key=" + key;
        parametros += "&anioinicio=" + Partefecha(fechainicio, "ano");
        parametros += "&mesinicio=" + Partefecha(fechainicio, "mes");
        parametros += "&diainicio=" + Partefecha(fechainicio, "dia");
        parametros += "&aniofin=" + Partefecha(fechafin, "ano");
        parametros += "&mesfin=" + Partefecha(fechafin, "mes");
        parametros += "&diafin=" + Partefecha(fechafin, "dia");

        String urlGET = wsURL + parametros;

        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(urlGET);
            get.setHeader("Content-type", "application/json");
            HttpResponse responseST = httpClient.execute(get);
            respuesta=responseST.toString();

            HttpEntity entityResult = responseST.getEntity();
            if (entityResult != null) {
                InputStream instream = entityResult.getContent();
                String resultadoRequest = convertStreamToString(instream);
                instream.close();
                respuesta = resultadoRequest;
    
                
            } else {
                respuesta = "error";
            }
        } catch (IOException | UnsupportedOperationException ex) {
            System.out.println("respuesta " + ex);
            respuesta = "error";
        }

        return respuesta;

    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            System.out.println("readline " + e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                System.out.println("close " + e);
            }

        }

        return sb.toString();

    }

    public int Partefecha(String fecha, String tipovalor) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
        Date fechanueva = null;
        int partefecha = 0;

        try {

            fechanueva = formatoDelTexto.parse(fecha);

            Calendar calendario = Calendar.getInstance();
            calendario.setTime(fechanueva);

            switch (tipovalor) {

                case "mes":
                    partefecha = calendario.get(Calendar.MONTH) + 1;
                    break;

                case "ano":
                    partefecha = calendario.get(Calendar.YEAR);
                    break;

                case "dia":
                    partefecha = calendario.get(Calendar.DAY_OF_MONTH);
                    break;

                default:
                    System.out.println("opcion invalida");
                    break;
            }

        } catch (ParseException ex) {
            System.out.println("Error al convertir fecha, Parte fecha " + ex.toString());
        }

        return partefecha;

    }

}
