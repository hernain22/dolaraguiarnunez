package gobws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class Gobws {

    private final String wsURL = "https://api.desarrolladores.datos.gob.cl/indicadores-financieros/v1/dolar/hoy.json/?auth_key=";
    private String key = "10217b03edd7a780a17949d6513215520ca2ec64";

    public String DolarHoy() throws MalformedURLException, IOException {

        String respuesta = "";
        String urlGET = wsURL + key;

        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(urlGET);
            get.setHeader("Content-type", "application/json");
            HttpResponse responseST = httpClient.execute(get);
            
            HttpEntity entityResult = responseST.getEntity();
            if (entityResult != null) {
                InputStream instream = entityResult.getContent();
                String resultadoRequest = convertStreamToString(instream);
                instream.close();
                respuesta = resultadoRequest;
            } else {
                respuesta = "error";
            }
        } catch (IOException | UnsupportedOperationException ex) {
           // System.out.println("respuesta " + ex);
            respuesta = "Es fin de semana";
        }

        return respuesta;

    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            System.out.println("readline " + e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                System.out.println("close " + e);
            }

        }

        return sb.toString();

    }

}
